import { readFileSync } from "fs";
import CartParser from "./CartParser";
import { expect } from "@jest/globals";

let parser, parse, validate, parseLine, calcTotal;

beforeEach(() => {
  parser = new CartParser();
  parse = parser.parse.bind(parser);
  validate = parser.validate.bind(parser);
  parseLine = parser.parseLine.bind(parser);
  calcTotal = parser.calcTotal.bind(parser);
});

describe("CartParser - unit tests", () => {
  it("empty csv file should return TypeError", () => {
    expect(() => validate("")).toThrow(TypeError);
  });

  it("empty header at csv table (column 1) should return object with error", () => {
    expect(validate("Product name,,Quantity")).toEqual([
      {
        column: 1,
        message: 'Expected header to be named "Price" but received .',
        row: 0,
        type: "header",
      },
    ]);
  });

  it("wrong header at csv table (column 2) should return object with error", () => {
    expect(validate("Product name,Price,Quality")).toEqual([
      {
        column: 2,
        message: 'Expected header to be named "Quantity" but received Quality.',
        row: 0,
        type: "header",
      },
    ]);
  });

  it("empty csv table (only with headers) should return empty array", () => {
    expect(validate("Product name,Price,Quantity")).toEqual([]);
  });

  it("empty cell at Product name column should return object with error", () => {
    expect(validate("Product name,Price,Quantity\n,250,1")).toEqual([
      {
        column: 0,
        message: 'Expected cell to be a nonempty string but received "".',
        row: 1,
        type: "cell",
      },
    ]);
  });

  it("negative number at Price column should return object with error", () => {
    expect(
      validate("Product name,Price,Quantity\nSamsung SSD 980,-20,1")
    ).toEqual([
      {
        column: 1,
        message: 'Expected cell to be a positive number but received "-20".',
        row: 1,
        type: "cell",
      },
    ]);
  });

  it("correctly formed csv table should return empty array", () => {
    expect(validate("Product name,Price,Quantity\nRTX 3070Ti,2000,1")).toEqual(
      []
    );
  });

  it("string at Price column should return NaN", () => {
    expect(parseLine("Ryzen 9 5900x,string,1")).toHaveProperty("price", NaN);
  });

  it("string at Quantity column should return NaN", () => {
    expect(parseLine("Ryzen 9 5900x,700,string")).toHaveProperty(
      "quantity",
      NaN
    );
  });

  it("correctly formed csv line should return correct object", () => {
    expect(parseLine("Ryzen 9 5900x,700,1")).toMatchObject({
      name: "Ryzen 9 5900x",
      price: 700,
      quantity: 1,
    });
  });

  it("should return sum of all items prices in the cart", () => {
    const cartItems = [
      {
        id: "3e6def17-5e87-4f27-b6b8-ae78948523a9",
        name: "Mollis consequat",
        price: 9,
        quantity: 2,
      },
      {
        id: "90cd22aa-8bcf-4510-a18d-ec14656d1f6a",
        name: "Tvoluptatem",
        price: 10.32,
        quantity: 1,
      },
      {
        id: "33c14844-8cae-4acd-91ed-6209a6c0bc31",
        name: "Scelerisque lacinia",
        price: 18.9,
        quantity: 1,
      },
      {
        id: "f089a251-a563-46ef-b27b-5c9f6dd0afd3",
        name: "Consectetur adipiscing",
        price: 28.72,
        quantity: 10,
      },
      {
        id: "0d1cbe5e-3de6-4f6a-9c53-bab32c168fbf",
        name: "Condimentum aliquet",
        price: 13.9,
        quantity: 1,
      },
    ];

    expect(calcTotal(cartItems)).toBeCloseTo(348.32);
  });
});

describe("CartParser - integration test", () => {
  it("should return object with products from cart.csv", () => {
    const uuidRegExp = new RegExp(
      /^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i
    );
    const cart = JSON.parse(
      readFileSync(
        "E:\\Programming\\BinaryStudioAcademy\\cart-parser-testing\\samples\\cart.json"
      )
    );
    for (const item of cart["items"]) {
      expect(item.id).toMatch(uuidRegExp);
      delete item.id;
    }

    expect(
      parse(
        "E:\\Programming\\BinaryStudioAcademy\\cart-parser-testing\\samples\\cart.csv"
      )
    ).toMatchObject(cart);
  });
});
